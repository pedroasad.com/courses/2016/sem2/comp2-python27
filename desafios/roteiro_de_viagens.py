# coding: utf-8

# Cada entrada do dicionário representa uma rota possível entre duas cidades.
# Cada chave é uma tupla da forma ('origem', 'destino') e cada valor
# correspondente contém a distância (em Km) e o tempo de viagem (em min). Estes
# dados foram retirados manualmente do Google maps e não representam,
# necessariamente, o caminho mais curtos ou mais eficiente entre os dois pontos.
rotas = {
	(     'Rio de Janeiro',  'Angra dos Reis') : (167.0, 150),
	(     'Angra dos Reis',          'Paraty') : (  9.0,  93),
	(             'Paraty',         'Ubatuba') : (  7.0,  67),
	(            'Ubatuba',           'Cunha') : ( 11.0, 101),
	(     'Rio de Janeiro',   'Volta Redonda') : ( 12.0, 107),
	(      'Volta Redonda',          'Penedo') : (  6.0,  53),
	(           'Itatiaia',          'Penedo') : (  1.0,  19),
	(      'Volta Redonda',        'Itatiaia') : (  6.0,  47),
	(      'Volta Redonda',  'Angra dos Reis') : (  9.0, 102),
	(     'Miguel Pereira',   'Volta Redonda') : (  8.0,  86),
	(     'Miguel Pereira',  'Rio de Janeiro') : ( 13.0, 107),
	(     'Rio de Janeiro',         'Niteroi') : (  2.0,  23),
	('Sao Pedro da Aldeia',         'Niteroi') : ( 12.0,  99),
	('Sao Pedro da Aldeia',       'Cabo Frio') : (  1.0,  21),
	(          'Cabo Frio', 'Arraial do Cabo') : (  1.0,  17),
	(             'Buzios',       'Cabo Frio') : (  2.0,  37),
	(     'Rio das Ostras',   'Nova Friburgo') : ( 10.0,  97),
	(     'Rio das Ostras',       'Cabo Frio') : (  6.0,  66),
	(      'Nova Friburgo',     'Teresopolis') : (  7.0,  81),
	(     'Rio de Janeiro',     'Teresopolis') : (  9.0,  85),
	(            'Niteroi',     'Teresopolis') : ( 10.0, 103),
	(     'Miguel Pereira',     'Teresopolis') : (  9.0, 108)
}

def fechamentoReflexivo(rotas):
	"""Computa o fechamento reflexivo de um dicionário de rotas.

	Assume-se que o dicionário `rotas` contenha entradas da forma

		(origem, destino) : (distancia, tempo)

	Neste caso, assegura-se que o dicionário retornado contenha, além de cada
entrada no formato anterior, a entrada com origem e destino trocados, ou seja,
ambos

		(origem, destino) : (distancia, tempo)
		(destino, origem) : (distancia, tempo)

façam parte do dicionário retornado.
	"""
	fecho = {}

	for (orig, dest), (dist, temp) in dict.items(rotas):
		fecho[orig, dest] = dist, temp
		fecho[dest, orig] = dist, temp

	return fecho

def questao_1():
	fecho = fechamentoReflexivo(rotas)
	print '%d rotas na lista original, %d rotas na lista aumentada' % (len(rotas), len(fecho))

# Ao final do programa principal, substituímos o dicionário de rotas pelo seu
# equivalente reflexivamente fechado. Todos os usos deste dicionário nas
# próximas questões poupa a aplicação deste procedimento, que é custoso,
# repetidas vezes.
rotas = fechamentoReflexivo(rotas)

def cidadesDisponiveis(rotas):
	"""Calcula o conjunto de cidades que aparecem no mapa de rotas dado.

	Toda e qualquer cidade que apareça como origem ou destino de alguma rota
será listada uma e apenas uma vez no conjunto retornado.
	"""
	cidades = []

	for origdest in rotas:
		cidades += list(origdest)

	return set(cidades)

def questao_2():
	cidades = sorted(cidadesDisponiveis(rotas))
	print 'As', len(cidades), 'cidades disponíveis são:', str.join(', ', cidades)


def cidadesVizinhas(cidade, rotas):
	"""Calcula as cidades vizinhas (imediatamente adjacentes) a uma cidade dada.
	"""
	vizinhas = []

	for orig, dest in rotas:
		if orig == cidade:
			vizinhas += [dest]

	return set(vizinhas)

def questao_3():
	cidades = sorted(cidadesDisponiveis(rotas))
	for c in cidades:
		vizinhas = sorted(cidadesVizinhas(c, rotas))
		print 'Cidades vizinhas a', c, ':', str.join(', ', vizinhas)

def cidadesAlcancaveis(cidade, viagens, rotas):
	"""Retorna um conjunto de cidades alcançáveis a partir de uma cidade dada,
com um número máximo de viagens entre cidades.

	Todas as cidades vizinhas a `cidade` são alcançáveis com uma viagem. Nenhuma
cidade é alcançável com 0 ou menos viagens. As cidades vizinhas às cidades
vizinhas de `cidade` são alcançáveis com 2 viagens e etc.
	"""

	# As cidades vizinhas são, evidentemente, alcançáveis com pelo menos uma
	# viagem.
	if viagens > 0:
		alcancaveis = list(cidadesVizinhas(cidade, rotas))
	else:
		alcancaveis = []

	# Para cada viagem adicional de que dispomos, basta aumentar a lista com as
	# cidades vizinhas às cidades já alcançáveis.
	while viagens > 1:
		novas_cidades = []
		for c in alcancaveis:
			novas_cidades += cidadesVizinhas(c, rotas)

		alcancaveis += novas_cidades
		viagens      -= 1

	# A função set(...) garante que retornemos apenas uma ocorrência de cada
	# cidade.
	return set(alcancaveis)

def questao_4():
	for saltos in range(1, 9):
		destinos = sorted(cidadesAlcancaveis('Ubatuba', saltos, rotas))
		print len(destinos), 'cidades alcançáveis de Ubatuba com', saltos, 'viagens:', str.join(', ', destinos)

def cidadesVizinhas(cidade, rotas):
	"""Calcula as cidades vizinhas (imediatamente adjacentes) a uma cidade dada.

	O dicionário retornado contém um par `(distância, tempo)` para cada cidade
vizinha a `cidade`. Em outras palavras, cada entrada da forma

		(cidade, v, d, t)

presente em `rotas` dá origem a uma entrada da forma

		(v, d, t)

no dicionário de vizinhos retornado
	"""
	vizinhas = {}

	for (orig, dest), (dist, temp) in dict.items(rotas):
		if orig == cidade:
			vizinhas[dest] = dist, temp

	return vizinhas

def cidadesAlcancaveis(cidade, viagens, rotas):
	"""Retorna um conjunto de cidades alcançáveis a partir de uma cidade dada,
com um número máximo de viagens entre cidades.

	Todas as cidades vizinhas a `cidade` são alcançáveis com uma viagem. Nenhuma
cidade é alcançável com 0 ou menos viagens. As cidades vizinhas às cidades
vizinhas de `cidade` são alcançáveis com 2 viagens e etc. O dicionário retornado
contém a distância e o tempo de viagem totais para cada cidade alcançável, mas
nenhuma informação sobre as cidades, distâncias ou tempos de viagem
intermediários. A distância até o destino é, garantidamente, a da menor rota.
	"""
	# As cidades vizinhas são, evidentemente, alcançáveis com pelo menos uma
	# viagem.
	if viagens > 0:
		alcancaveis = cidadesVizinhas(cidade, rotas)
	else:
		alcancaveis = {}

	# Este passo é sutil mas importante: é incorreto (não funciona) iterar
	# diretamente sobre o dicionário `alcancaveis`, pois este dicionário será
	# modificado durante a iteração pela adição de novas cidades. Por isso,
	# extraímos as cidades vizinhas para uma lista separada que não se modifica.
	vizinhas = dict.keys(alcancaveis)

	# Como as cidades vizinhas são diretamente alcançáveis e conhecemos a
	# distância e o tempo de viagem até elas, podemos aumentar nosso repertório
	# olhando para as cidades alcançáveis a partir das mesmas com um número de
	# viagens reduzido de 1. Esta recursão é interrompida quando chamamos a
	# função com `viagens` igual a 0. Mas é importante verificar, a cada passo,
	# se já não foi encontrado um trajeto melhor (menor distância, mas poderia
	# ser outro critério) para cada nova candidata.
	for c in vizinhas:
		dist_c, temp_c = alcancaveis[c]
		indiretas = cidadesAlcancaveis(c, viagens - 1, rotas)
		for i, (dist_i, temp_i) in dict.items(indiretas):
			dist_i += dist_c
			temp_i += temp_c
			if i not in alcancaveis or alcancaveis[i][0] > dist_i:
				alcancaveis[i] = dist_i, temp_i

	# Não é necessário usar a função set(...) como na versão anterior. Afinal,
	# não há chaves repetidas em um dicionário.
	return alcancaveis

def questao_5():
	for saltos in range(1, 9):
		destinos = cidadesAlcancaveis('Ubatuba', saltos, rotas)
		textos_destinos = []
		for c in destinos:
			dist, temp = destinos[c]
			textos_destinos += ['%s (%d Km, %d min)' % (c, dist, temp)]
		textos_destinos = sorted(textos_destinos)
		print len(destinos), 'cidades alcançáveis de Ubatuba com', saltos, 'viagens:', str.join(', ', textos_destinos)

def distanciaEntre(origem, destino, rotas, maior_rota = 0.5):
	"""Computa a menor distância e o tempo correspondente de viagem entre um par de
cidades, dado um mapa de rotas e um limite de viagens entre cidades.

	`maior_rota` deve ser um número de 0.0 a 1.0 representando a fração máxima
do número total de cidades que pode ser percorrido para encontrar o caminho. O
valor padrão, 0.5, significa que, no máximo, metade das cidades pode ser
percorrida para traçar uma rota. Se não houver rota possível nas condições
especificadas, é retornado o par (inf, inf).
	"""
	maior_rota = int(len(cidadesDisponiveis(rotas)) * maior_rota)
	alcancaveis = cidadesAlcancaveis(origem, maior_rota, rotas)

	if destino in alcancaveis:
		return alcancaveis[destino]
	else:
		return (float('inf'), float('inf'))

def questao_6():
	cidades = cidadesDisponiveis(rotas)
	textos = []
	for orig in cidades:
		for dest in cidades: 
			if dest != orig:
				dist, temp = distanciaEntre(orig, dest, rotas)
				print orig, dest, dist, temp
				textos += [orig + ' a ' + dest + ': ' + str(dist) + ' Km, ' + str(temp) + ' min']
