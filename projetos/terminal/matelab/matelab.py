# coding: utf-8 #
import lista_01 as kit
from leao import leao
from math import *

def tela_nova(largura, altura, intervalo_x, intervalo_y):

    margem_x = 1

    margem_y = kit.maximo([
        str(intervalo_y[ 0]), 
        str(intervalo_y[-1])
    ]) + 1

    tela = [[' ' for j in range(altura + margem_y)] for i in range(largura + margem_x)]

    return margem_x, margem_y, tela

def plot(parametros):
    funcao = eval('lambda x: ' + kit.juncao_strings(parametros, ''))
    valores_x = kit.amostras_inc(intervalo_x[0], intervalo_x[1], resolucao)
    valores_y = kit.mapear(valores_x, funcao)
    pontos = zip(valores_x, valores_y)

    escala_x = (tam_tela[0] - 1.0) / (intervalo_x[1] - intervalo_x[0])
    escala_y = (tam_tela[1] - 1.0) / (intervalo_y[1] - intervalo_y[0])

    tela = tela_nova()

    for p in pontos:
        x, y = p

        if not (intervalo_y[0] <= y <= intervalo_y[1]):
            continue

        i = tam_tela[0] - int((x - intervalo_x[0]) * escala_x) - 1 + margem_y
        j = int((x - intervalo_x[1]) * escala_y) + margem_x

        tela[i][j] = '*'

    tela = '\n'.join([str.join('', linha) for linha in tela])
    print tela

# Prograna principal começa aqui
resolucao = 0.1
intervalo_x = [-5, 5]
intervalo_y = [-5, 5]
tam_tela = (40, 120)
margem_x = 1
margem_y = 1

print leao

while True:
    linha = raw_input('matelab > ')
    linha = kit.separar(linha, ' ')
    linha = kit.remover_todos(linha, '')

    comando = str.lower(linha[0])
    parametros = kit.fatia(linha, 1)

    if comando == 'plot':
        plot(parametros)
    elif comando == 'quit':
        break
