from objetos import Bola, Mesa, Taco
       
mesa = Mesa()
taco = Taco(mesa)    
    
def setup():
    size(900, 450)
    mesa.arrumar('tradicional')
        
def mouseClicked():
    if mesa.parada():
        taco.disparar()
    
def draw():
    mesa.desenhar()
    taco.desenhar()
    
    for b in mesa.bolas:
        b.desenhar()
        b.atualizar(1 / frameRate)
