positions = []
velocities = []
fps = 30.0
escala = 40.0 / 960

segundo = fps
metro = 1 / escala

ac_grav = 9.87 * metro / segundo**2

def setup():
    size(200, 200)
    
    frameRate(fps)
    
    global positions
    global velocities
    
    for i in range(20):
        positions .append([random(width), random(height)])
        velocities.append([0.0, -random(10.0)])
    
def draw():
    background(0)
    
    stroke(255)
    strokeWeight(3)
    for p, v in zip(positions, velocities):
        point(p[0], p[1])
        v[1] += ac_grav
        p[0] += v[0]
        p[1] += v[1]
        if p[1] >= height:
            v[1] *= -0.8
