import fisica
    
class Bola:
    COMUM = 1
    QUADRADA = 2
    
    def __init__(self, raio, pos, tipo = COMUM):
        self.raio = raio
        self.pos = pos
        self.vel = PVector(0.0, 0.0)
        self.tipo = tipo
    
    def atualizar(self, delta_tempo):
        self.pos += self.vel * delta_tempo
        self.vel += PVector(0, fisica.gravidade) * delta_tempo
        
        if self.pos.y > height - self.raio:
            self.pos.y = height - self.raio
            self.vel.y *= -0.9
        
    def desenhar(self):
        noStroke()
        fill(50, 200, 100)
        
        if self.tipo == Bola.COMUM:
            ellipseMode(CENTER)
            ellipse(self.pos.x, self.pos.y, 2 * self.raio, 2 * self.raio)
        elif self.tipo == Bola.QUADRADA:
            rectMode(CENTER)
            rect(self.pos.x, self.pos.y, 2 * self.raio, 2 * self.raio, self.raio / 2)
        
desenhaveis = []
atualizaveis = []

def setup():
    size(800, 600)
    
def mouseClicked():
    if mouseButton == LEFT:
        b = Bola(random(10, 30), PVector(mouseX, mouseY), Bola.COMUM)
        desenhaveis .append(b)
        atualizaveis.append(b)
    elif mouseButton == RIGHT:
        b = Bola(random(10, 30), PVector(mouseX, mouseY), Bola.QUADRADA)
        desenhaveis .append(b)
        atualizaveis.append(b)
        
def draw():
    background(0)
    
    for ob in desenhaveis:
        ob.desenhar()
        
    for ob in atualizaveis:
        ob.atualizar(1 / frameRate)
