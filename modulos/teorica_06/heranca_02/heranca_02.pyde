import fisica

class Quicante(object):
    def __init__(self, raio, pos):
        self.raio = raio
        self.pos = pos
        self.vel = PVector(0.0, 0.0)
        
    def atualizar(self, delta_tempo):
        self.pos += self.vel * delta_tempo
        self.vel += PVector(0, fisica.gravidade) * delta_tempo
        
        if self.pos.y > height - self.raio:
            self.pos.y = height - self.raio
            self.vel.y *= -0.9
    
class Bola(Quicante):
    def __init__(self, raio, pos):
        super(Bola, self).__init__(raio, pos)
        
    def desenhar(self):
        noStroke()
        fill(50, 200, 100)
        ellipseMode(CENTER)
        ellipse(self.pos.x, self.pos.y, 2 * self.raio, 2 * self.raio)
        
class BolaQuadrada(Quicante):
    def __init__(self, raio, pos):
        super(BolaQuadrada, self).__init__(raio, pos)
        
    def desenhar(self):
        noStroke()
        fill(50, 200, 100)
        rectMode(CENTER)
        rect(self.pos.x, self.pos.y, 2 * self.raio, 2 * self.raio, self.raio / 2)
        
desenhaveis = []
atualizaveis = []

def setup():
    size(800, 600)
    
def mouseClicked():
    if mouseButton == LEFT:
        b = Bola(random(10, 30), PVector(mouseX, mouseY))
        desenhaveis .append(b)
        atualizaveis.append(b)
    elif mouseButton == RIGHT:
        b = BolaQuadrada(random(10, 30), PVector(mouseX, mouseY))
        desenhaveis .append(b)
        atualizaveis.append(b)
        
def draw():
    background(0)
    
    for ob in desenhaveis:
        ob.desenhar()
        
    for ob in atualizaveis:
        ob.atualizar(1 / frameRate)
