# coding:utf-8

from roteiro import *

def teste_Hora():
	h1 = Hora(20, 30, 60)
	assert str(h1) == '20:31:00'

	h2 = Hora(24, 0, 0)
	assert str(h2) == '00:00:00'

	h3 = Hora(24, 60, 60)
	assert str(h3) == '01:01:00'

def teste_horaMaisDeltaTempo():
	h1 = Hora(20, 30, 59)

	d1 = DeltaTempo(1, 30, 1)
	d2 = DeltaTempo(-1, -30, 60)

	h1d1 = horaMaisDeltaTempo(h1, d1)
	h1d2 = horaMaisDeltaTempo(h1, d2)

	assert str(h1d1) == '22:01:00'
	assert str(h1d2) == '19:01:59'

teste_Hora()
teste_horaMaisDeltaTempo()
