# coding:utf-8 #

# Esta classe é apenas um exemplo de como definimos o inicializador __init__ que
# é chamado pelo construtor da classe, uma função especial que sempre leva o
# mesmo nome da classe -- neste caso, Data().
class Data:
	def __init__(data, d, m, a):
		data.dia = d
		data.mes = m
		data.ano = a

	def __str__(data):
		return '%04d-%02d-%02d' % (data.ano, data.mes, data.dia)

# Esta classe que representa Horas com resolução de segundos.
class Hora:
	def __init__(x, h, m, s):
                pass

	# Esta é uma função especial que converte do tipo Hora para o tipo str, ou
	# seja, transforma um objeto Hora e um objeto string.
	def __str__(hora):
		return '%02d:%02d:%02d' % (hora.horas, hora.minutos, hora.segundos)	

# Esta classe também é um exemplo: mostra como combinar as classes Data e Hora
# para representar estes dois aspectos do tempo.
class DataHora:
	def __init__(dh, dia, mes, ano, h, m, s):
		dh.data = Data(dia, mes, ano)
		dh.hora = Hora(h, m, s)

class DeltaTempo:
	def __init__(dt, h, m, s):
		pass

def horaMaisDeltaTempo(hora, delta):
	pass

# Esta função fica como sugestão. Como combinar esta função com aquilo que já
# fizemos em termos de calendário nas aulas passadas? Seria interessante
# generalizar primeiro o tipo DeltaTempo para lidar com distâncias em dias e
# segundos, que são as menores unidades representadas por Data e Hora,
# respectivamente. Escreva alguns testes para isso.
def dataHoraMaisDeltaTempo(dh, delta):
	pass
