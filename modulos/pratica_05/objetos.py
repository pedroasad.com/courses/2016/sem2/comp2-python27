class Bola:
    cores = [
        (255,   0,   0), # Vermelha
        (255, 255,   0), # Amarela
        (  0, 255,   0), # Verde
        (150,  50,  20), # Marrom
        (  0,   0, 255), # Azul
        (255, 200, 200), # Rosa
        (  0,   0,   0)  # Preta
    ]
    
    raio = 0.03 # 3cm

    def __init__(self, num, pos):
        self.cor = cor
        self.pos = pos
        
    def cor(self):
        return Bola.cores[self.num]
        
    def atualizar(self, delta_tempo):
        pass
        
    def desenhar(self):
        noStroke()
        fill(self.cor[0], self.cor[1], self.cor[2])
        ellipseMode(CENTER)
        ellipse(self.pos[0], self.pos[1], 2 * Bola.raio, 2 * Bola.raio)
        
        fill(255)
        ellipse(self.pos[0], self.pos[1], 1.5 * Bola.raio, 1.5 * Bola.raio)
        
        fill(0)
        textMode(CENTER)
        text(self.num)
        
class Mesa:
    largura = 2
    altura  = 1
    
    def desenhar(self):
        background(0, 100, 0)
        
class Lampada:
    raio_max = 200.0
    alfa_min =   1.0
    alfa_max = 100.0
    
    @staticmethod
    def intensidade(raio):
        return (Lampada.alfa_max - Lampada.alfa_min) * (Lampada.raio_max * (1 / raio) - 1) / (Lampada.raio_max - 1) + Lampada.alfa_min
    
    def desenhar(self):
        
        noStroke()
        ellipseMode(CENTER)        
        
        for raio in range(int(Lampada.raio_max), 0, -1):
            fill(255, Lampada.intensidade(raio))
            ellipse(width / 2, height / 2, 2 * raio, 2 * raio)

