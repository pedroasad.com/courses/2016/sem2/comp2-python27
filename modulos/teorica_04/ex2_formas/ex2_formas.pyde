size(800, 500)

background(120)

fill(255)
ellipse(300, 250, 50, 100)
ellipse(350, 250, 50, 100)

fill(0)
ellipse(310, 265, 20, 20)
ellipse(360, 265, 20, 20)

fill(255, 200, 0)
noStroke()
arc(500, 250, 150, 150, PI + PI / 6, 3 * PI - PI/6)

stroke(255, 0, 0)
strokeWeight(3)
line(220, 250, 200, 250)
line(200, 250, 200, 100)
point(200, 90)
point(200, 80)
point(200, 70)

stroke(0, 0, 100)
noFill()
beginShape()
vertex(600, 100)
vertex(700, 150)
vertex(750, 300)
vertex(600, 250)
vertex(650, 220)
vertex(550, 50)
endShape(CLOSE)

saveFrame('processing_02.png')

