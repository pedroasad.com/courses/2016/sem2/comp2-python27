"""Homer Simpson

baseado no Homer Simpson piscando, feito por 
Ronaldo e Nehrer (turma EN1/ET1/ER1 de 2015-1).
"""

def setup():
    size(800,800)
    background(250,250,0)
    
    fill(255)
    arc(300,200,150,150,-PI/6,PI+PI/3,CLOSE)
    arc(500,200,150,150,-PI/3,PI+PI/6,CLOSE)
    
    fill(0)
    ellipse(300,200,30,30)
    ellipse(500,200,30,30)
    
    fill(133,88,19,200)
    ellipse(400,475,300,250)
    
    noFill()
    arc(400,300,80,80,-PI/6,PI+(PI/3),OPEN)
    
    noFill()
    arc(400,500,200,80,PI, PI*2,OPEN)
