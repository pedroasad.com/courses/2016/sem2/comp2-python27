posicao = [0, 0]

def setup():
    size(800, 600)
    frameRate(30)
    
    posicao[0] = width  - width / 10
    posicao[1] = height / 4

def draw():
    background(120)
    
    fill(255, 200, 0); noStroke()
    arc(posicao[0], posicao[1], 150, 150, PI + PI / 6, 3 * PI - PI/6)
    
    fill(0)
    ellipse(posicao[0] - 20, posicao[1] - 40, 20, 20)

    posicao[0] -= 2
