posicao = [0, 0]
raio_corpo = 75
raio_olho  = 10

fps = 30

def setup():
    size(800, 600)
    frameRate(fps)
    
    posicao[0] = width + raio_corpo
    posicao[1] = raio_corpo

def draw():
    tempo = float(frameCount) / fps
    abertura = (cos(2 * PI * tempo) + 1) / 2
    
    background(120)
    
    fill(255, 200, 0); noStroke()
    arc(posicao[0], posicao[1], 2 * raio_corpo, 2 * raio_corpo, PI + abertura * PI / 6, 3 * PI - abertura * PI/6)
    
    fill(0)
    ellipse(posicao[0] - 20, posicao[1] - 40, 2 * raio_olho, 2 * raio_olho)

    posicao[0] -= 4
    if posicao[0] < -raio_corpo:
        posicao[0] = width + raio_corpo
        posicao[1] += 100
        
    if posicao[1] >= height + raio_corpo:
        posicao[1] = raio_corpo

