class Seguidor:
    aceleracao_maxima = 500.0
    
    def __init__(self, pos):
        self.pos = pos
        self.vel = PVector(0.0, 0.0)
        self.alvo = None
    
    def atualizar(self, delta_tempo):
        self.pos += self.vel * delta_tempo
        self.vel += self.aceleracao() * delta_tempo
        
    def aceleracao(self):
        if self.alvo is None:
            return PVector(0.0, 0.0)
        else:
            direcao = self.alvo - self.pos
            return direcao
    
    def seguir(self, alvo):
        self.alvo = alvo
    
class Missil(Seguidor):
    imagem = None
    
    def desenhar(self):
        if Missil.imagem is None:
            Missil.imagem = loadImage('missil.png')
        
        translate(self.pos.x, self.pos.y)
        rotate(atan2(self.vel.y, self.vel.x))
        imageMode(CENTER)
        image(Missil.imagem, Missil.imagem.width / 2, 0)
        resetMatrix()
        
class MissilFogarento(Missil):
    def desenhar(self):
        strokeWeight(3)
        stroke(random(200, 255), random(70, 200), random(18, 70), 200)
        line(self.pos.x, self.pos.y, self.pos.x - self.vel.x / 10, self.pos.y - self.vel.y / 10)
        Missil.desenhar(self)
        
class MissilEsperto(MissilFogarento):
    velocidade_maxima = 1000.0
    
    def aceleracao(self):
        if self.alvo is None:
            return PVector(0.0, 0.0)
        else:
            distancia = self.alvo - (self.pos + self.vel)
            direcao = self.alvo - self.pos
            direcao.normalize()
            return MissilEsperto.velocidade_maxima * direcao - 2 * self.vel 
    
misseis = []  

def setup():
    size(800, 600)
    
    for i in range(1):
        m = Missil(PVector(random(width), random(height)))
        misseis.append(m)
    
def draw():
    background(0)
    
    for ob in misseis:
        ob.desenhar()
        ob.seguir(PVector(mouseX, mouseY))
        ob.atualizar(1 / frameRate)    
        
