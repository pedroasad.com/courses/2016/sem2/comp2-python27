import fisica

class Corpo:
    def __init__(self, pos, vel = PVector(0.0, 0.0)):
        self.pos = pos
        self.vel = vel
        
    def atualizar(self, delta_tempo):
        self.pos += self.vel * delta_tempo
        self.vel += fisica.gravidade * delta_tempo
        
class Confeti(Corpo):
    raio = 4
    
    def desenhar(self):
        noStroke()
        fill(random(255), random(255), random(255))
        ellipse(self.pos.x, self.pos.y, 2 * Confeti.raio, 2 * Confeti.raio)
        
class Flecha(Confeti):
    comprimento = 32
    
    def desenhar(self):
        strokeWeight(3)
        stroke(random(255), random(255), random(255))
        fill(random(255), random(255), random(255))
        
        v = self.comprimento * self.vel / self.vel.mag()
        line(self.pos.x, self.pos.y, self.pos.x + v.x, self.pos.y + v.y)
        
desenhaveis = []
atualizaveis = []
        
def setup():
    size(800, 600)
    
def mouseClicked():
    n = int(random(5, 15))
    for i in range(n):
        p = PVector(mouseX, mouseY)
        v = PVector(300 * cos(i * TWO_PI / n), 300 * sin(i * TWO_PI / n))
        
        c = Flecha(p, v)
        
        desenhaveis .append(c)
        atualizaveis.append(c)
        
def draw():
    background(0)
    
    for ob in desenhaveis:
        ob.desenhar()
        
    for ob in atualizaveis:
        ob.atualizar(1 / frameRate)
