import pdb
def ehBissexto(ano):
	if ano > 1582:
		if ano % 100 == 0:
			return ano % 400 == 0
		else:
			return ano % 4 == 0
	else:
		return ano % 4 == 0

def diasNoMes(mes, ano):
	if ano == 1582 and mes == 10:
		return 21
	else:
		if ehBissexto(ano):
			dias = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
		else:
			dias = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

		return dias[mes - 1]

def diasNoAno(ano):
	if ano == 1582:
		return 355
	elif ehBissexto(ano):
		return 366
	else:
		return 365

def diasAteOMes(mes, ano):
	dias = 0
	for m in range(1, mes):
		dias += diasNoMes(m, ano)
	return dias

def diaDoAno(dia, mes, ano):
	return dia + diasAteOMes(mes, ano)

def diaDaSemana(dia, mes, ano):
	return (3 + diasEntre((22, 4, 2015), (dia, mes, ano))) % 7 + 1

def anoMesDia(data):
	return '%04d-%02d-%02d'	% (data[2], data[1], data[0])

def diasEntre(data_1, data_2):
	if anoMesDia(data_1) > anoMesDia(data_2):
		data_1, data_2 = data_2, data_1
		fator = -1
	else:
		fator = 1

	dia_1, mes_1, ano_1 = data_1
	dia_2, mes_2, ano_2 = data_2

	if ano_1 == ano_2:
		return diaDoAno(dia_2, mes_2, ano_2) - diaDoAno(dia_1, mes_1, ano_1)
	else:
		dias = 0
		dias += diasNoAno(ano_1) - diaDoAno(dia_1, mes_1, ano_1)
		dias += diaDoAno(dia_2, mes_2, ano_2)

		for a in range(ano_1 + 1, ano_2):
			dias += diasNoAno(a)

		return dias * fator
