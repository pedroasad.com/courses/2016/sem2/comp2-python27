# coding=utf-8
#
# Código para exemplificar o uso do Pdb (Python debugger). Em suma, a função
# principal imprime uma lista de `n` valores gerada pela função `valores(n)`.
# Esta lista é populada com a soma dos valores de uma parábola e uma cúbica
# variando de 0 a `n`. 
#
# O estilo "inobjetivo" do código é para possibilitar a exploração da pilha de
# chamadas e das instruções de navegação do Pdb. Um bom exemplo prático de uso
# do pdb pode ser visto aqui:
# 	http://pymotw.com/2/pdb/

from sys import argv

def quadrado(x):
	valor = x ** 2
	return valor

def cubo(x):
	valor = x ** 3
	return valor

def parabola(x, a = 1, b = 1, c = 1):
	return a * quadrado(x) + b * x + c

def cubica(x, a = 1, b = 1, c = 1, d = 1):
	return a * cubo(x) + b * quadrado(x) + c * x + d

def valores(n):
	lista = []
	for x in range(n):
		y1 = parabola(x, 2, -3, -5)
		y2 = cubica(x, 2, -3, -5, 0)
		lista = lista + [y1 + y2]
	return lista

def main():
	if len(argv) > 1:
		n = int(argv[1])
	else:
		n = 10

	print valores(n)

if __name__ == '__main__':
	main()
