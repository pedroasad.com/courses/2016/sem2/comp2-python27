# piscadelas que seguem o mouse
# chuva com relâmpagos
#

def setup():
    size(600, 500)
    
    background(100)
    
    fill(255)
    ellipse(200, 200, 50, 100)
    ellipse(250, 200, 50, 100)
    
    fill(0)
    ellipse(210, 210, 20, 20)
    ellipse(260, 210, 20, 20)
    
    fill(255)
    arc(225, 270, 150, 75, 0, PI, OPEN)
    
    
    
    fill(255)
    ellipse(400, 200, 50, 100)
    ellipse(450, 200, 50, 100)
    
    fill(100)
    arc(400, 200, 50, 100, PI + PI/6, 2 * PI - PI/6, OPEN)
    arc(450, 200, 50, 100, PI + PI/6, 2 * PI - PI/6, OPEN)
    
    fill(0)
    ellipse(410, 210, 20, 20)
    ellipse(460, 210, 20, 20)
    
    noFill()
    arc(425, 270, 150, 75, 0, PI, OPEN)
    
    saveFrame('piscadela.png')
