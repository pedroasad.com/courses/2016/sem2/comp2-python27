# coding: utf-8

# As datas inicial e final do curso são definidas usando duas tuplas, em que os
# elementos da posição 0 representam o dia e os elementos da posição 1
# representam o mês. Todas as datas seguirão este formato, a não ser quando
# tratarem exclusivamente do mês.
data_i = (17, 3)
data_f = (14, 7)

def diasEMesesEntre(data_1, data_2):
	"""Calcula a diferença entre duas datas no mesmo ano, considerando que todos os
meses tenham 30 dias.

	A diferença é calculada em termos de dias e meses. Assume-se que `data_2` é
uma data posterior a `data_1`.
	"""
	dias  = data_2[0] - data_1[0]
	meses = data_2[1] - data_1[1]

	if dias < 0:
		dias  += 30
		meses -= 1

	return (dias, meses)

def questao_1():
	print 'Há %d dias e %d meses entre os dias %d/%d e %d/%d' % (diasEMesesEntre(data_i, data_f) + data_i + data_f)

def diasEntre(data_1, data_2):
	"""Calcula a quantidade de dias entre duas datas, cosiderando que todos os meses
tenham 30 dias.

	Assume-se que `data_2` é posterior a `data_1`.
	"""
	(d, m) = diasEMesesEntre(data_1, data_2)
	return d + m * 30

def questao_2():
	print 'Há ' + str(diasEntre(data_i, data_f)) + ' dias entre os dias %d/%d e %d/%d' % (data_i + data_f)

def semanasEntre(data_1, data_2):
	"""Calcula a quantidade de semanas inteiras entre duas datas no mesmo ano,
cosiderando que todos os meses tenham 30 dias.

	Assume-se que `data_2` é posterior a `data_1`.
	"""
	return diasEntre(data_1, data_2) / 7

def questao_3():
	aulas_teo = semanasEntre(data_i, data_f) + 1
	aulas_pra = semanasEntre((data_i[0] + 2, data_i[1]), data_f) + 1
	print 'Haverá %d aulas teóricas e %d aulas práticas entre os dias %d/%d e %d/%d' % ((aulas_teo, aulas_pra) + data_i + data_f)

def diasNoMes(mes):
	"""Calcula a quantidade de dias em um mês, considerando que todos os anos tem 365
dias (não há anos bissextos).

	Assume-se que `mes` é um número de 1 a 12, em que 1 representa Janeiro e 12
representa Dezembro.
	"""
	dias_no_mes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	return dias_no_mes[mes - 1]

def diasEntre(data_1, data_2):
	"""Calcula a quantidade de dias entre duas datas, cosiderando que todos os anos
tem 365 dias (não há anos bissextos).

	Assume-se que `data_2` é posterior a `data_1`.
	"""
	# Iniciamos com nenhum dia e contamos a duração de problemas menores.
	dias = 0

	# Adicionamos a duração de cada mês posterior ao mês da data inicial e anterior ao
	# mês da data final, pois estes meses contribuem inteiros pra contagem. O intervalo
	# retornado pela função range() pode ser vazio, ou conter um único elemento,
	# eventualmente.
	for mes in range(data_1[1] + 1, data_2[1]):
		dias += diasNoMes(mes)

	# Do mês da data inicial, adicionamos apenas os dias restantes até o final deste
	# mesmo mês.
	dias += diasNoMes(data_1[1]) - data_1[0]

	# Se o mês da data final for diferente do mês da data inicial, adicionamos apenas
	# os dias passados neste mesmo mês.
	if data_2[1] > data_1[1]:
		dias += data_2[0]

	return dias

# ATENÇÃO: No arquivo questao_02_03.py, a função semanasEntre(...) retorna, para
# o nosso exemplo, o valor de 16 semanas, pois ela faz uso da função
# diasEntre(...) definida no arquivo questao_02_02.py que retorna 117 dias, por
# considerar que todos os meses tem 30 dias. Como importamos o módulo
# questao_02_03 no começo deste arquivo, a função semanasEntre(...) trazida de
# lá enxerga a definição antiga da função diasEntre(...) e não a nova versão que
# criamos neste arquivo. Por isso redefinimos, abaixo, a função
# semanasEntre(...) para assegurar que ela usará a versão nova (e correta) da
# função diasEntre(...) que retorna 119 dias para o nosso exemplo. A resposta,
# agora, estará correta: 17 semanas de intervalo e 18 dias de aula.

def semanasEntre(data_1, data_2):
	"""Calcula a quantidade de semanas inteiras entre duas datas no mesmo ano,
cosiderando que todos os anos tem 365 dias (não há anos bissextos).

	Assume-se que `data_2` é posterior a `data_1`.
	"""
	return diasEntre(data_1, data_2) / 7

def questao_4():
	aulas_teo = semanasEntre(data_i, data_f) + 1
	aulas_pra = semanasEntre((data_i[0] + 2, data_i[1]), data_f) + 1
	print 'Haverá %d aulas teóricas e %d aulas práticas entre os dias %d/%d e %d/%d' % ((aulas_teo, aulas_pra) + data_i + data_f)

def diasAteOMes(mes):
	"""Calcula a quantidade de dias, desde 1º de Janeiro, que antecedem o mês dado,
considerando que todos os anos tenham 365 dias (não haja anos bissextos)

	Por exemplo: o mês de Janeiro, não é precedido de nenhum dia; Fevereiro é
precedido por 31 dias; Março é precedido de 59 dias e etc.
	"""
	if mes == 1:
		return 0
	else:
		return diasAteOMes(mes - 1) + diasNoMes(mes - 1)

def questao_5():
	print 'Dias até o começo dos meses:'
	print 'Jan Fev Mar Abr Mai Jun Jul Ago Set Out Nov Dez'
	for mes in range(1, 13):
		print '%3d' % diasAteOMes(mes),
def diaDoAno(data):
	"""Converte uma data consistindo de dia e mês para um número de 1 a 365,
considerando que todos os anos tenham 365 dias (não haja anos bissextos).

	Por exemplo: o dia 1º de Janeiro corresponde ao número 1; 1º de Fevereiro ao
dia 32; 1º de Março ao dia 60 e etc.
	"""
	return diasAteOMes(data[1]) + data[0]

def questao_7():
	aulas_teo = (diaDoAno(data_f) - diaDoAno(data_i)) / 7 + 1
	aulas_pra = (diaDoAno(data_f) - diaDoAno((data_i[0] + 2, data_i[1]))) / 7 + 1
	print 'Haverá %d aulas teóricas e %d aulas práticas entre os dias %d/%d e %d/%d' % ((aulas_teo, aulas_pra) + data_i + data_f)

feriados_2015 = [(3, 4), (21, 4), (23, 4), (1, 5)]

def diasDeAula(data_1, data_2, feriados):
	"""Calcula a quantidade de dias de aula entre as datas fornecidas,
descontando os feriados informados.

	Assume-se que `data_2` seja posterior a `data_1`, que o primeiro dia de aula
ocorra em `data_1` e que haja apenas uma aula por semana.
	"""
	# Registramos o dia do ano para o primeiro e último dias de aula e a
	# previsão do total de aulas sem levar em conta os feriados, inicialmente.
	primeiro_dia =  diaDoAno(data_1)
	ultimo_dia   =  diaDoAno(data_2)
	dias_totais  = (diaDoAno(data_2) - diaDoAno(data_1)) / 7 + 1

	# Verificamos se cada um dos feriados cai no intervalo de datas fornecidas
	# e cai no mesmo dia da semana que o primeiro dia de aula (correspondente à
	# data inicial data_1). Saber se os dias da semana coicidem é tão simples
	# quanto verificar se os dias do ano tem o mesmo resto na divisão por 7.
	for f in feriados:
		f = diaDoAno(f)
		if primeiro_dia <= f <= ultimo_dia and primeiro_dia % 7 == f % 7:
			dias_totais -= 1

	return dias_totais

def questao_8():
	aulas_teo = diasDeAula(data_i, data_f, feriados_2015)
	aulas_pra = diasDeAula((data_i[0] + 2, data_i[1]), data_f, feriados_2015)
	print 'Hverá %d aulas teóricas e %d aulas práticas.' % (aulas_teo, aulas_pra)