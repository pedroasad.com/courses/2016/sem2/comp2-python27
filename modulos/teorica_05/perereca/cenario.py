escala    = 40.00 # pixels por metro
gravidade =  9.87 # m/s^2

largura = 20.0
altura  = 15.0

def pixels(medida):
    return int(medida * escala)

def metros(pixels):
    return pixels / escala

def frames(medida):
    return int(medida * frameRate)

def segundos(frames):
    return frames / frameRate
