import cenario
    
class Bola:
    def __init__(self, nome, raio, cor, pos, vel = (0.0, 0.0), elasticidade = 0.85):
        self.nome = nome
        self.raio = raio
        self.cor = cor
        self.pos_x, self.pos_y = pos
        self.vel_x, self.vel_y = vel
        self.elasticidade = elasticidade
        
    def atualizar(self, delta_tempo):
        self.pos_x += self.vel_x * delta_tempo
        self.pos_y += self.vel_y * delta_tempo
        
        if self.pos_x < self.raio:
            self.pos_x = self.raio
            self.vel_x *= -self.elasticidade
            
        if self.pos_x >= cenario.largura - self.raio:
            self.pos_x = cenario.largura - self.raio
            self.vel_x *= -self.elasticidade
        
        if self.pos_y >= cenario.altura - self.raio:
            self.pos_y = cenario.altura - self.raio
            self.vel_y *= -self.elasticidade
        
        self.vel_y += cenario.gravidade * delta_tempo
        
    def ao_redor_de(self, x, y):
        return (x - self.pos_x)**2 + (y - self.pos_y)**2 <= self.raio**2
        
    def desenhar(self):
        noStroke()
        fill(self.cor[0], self.cor[1], self.cor[2], self.cor[3])
        
        x = cenario.pixels(self.pos_x)
        y = cenario.pixels(self.pos_y)
        r = cenario.pixels(self.raio)
        
        textSize(18)
        text(self.nome, x - r, y - r)
        
        ellipseMode(CENTER)
        ellipse(x, y, 2 * r, 2 * r)
