import cenario
import random as rd
from objetos import Bola

min_raio = 0.6
max_raio = 1.5
bolas = []

cores = [(200, 50, 50, 255), (50, 200, 50, 255), (50, 50, 200, 255), (200, 200, 50, 255), (200, 50, 200, 255), (50, 200, 200, 255)]

def setup():
    size(cenario.pixels(cenario.largura), cenario.pixels(cenario.altura))
    
def mouseClicked():
    global bolas
    
    b = Bola(
        'Bola %d' % (len(bolas) + 1),
        random(min_raio, max_raio),
        rd.choice(cores),
        (cenario.metros(mouseX), cenario.metros(mouseY)))
    
    list.append(bolas, b)
    
def draw():
    background(0)
    
    for b in bolas:
        Bola.desenhar(b)
        Bola.atualizar(b, 1 / frameRate)
