import cenario
    
class Bola:
    def __init__(self, nome, raio, cor, pos):
        self.nome = nome
        self.raio = raio
        self.cor = cor
        self.pos_x, self.pos_y = pos
        
    def ao_redor_de(self, x, y):
        return (x - self.pos_x)**2 + (y - self.pos_y)**2 <= self.raio**2
        
    def desenhar(self):
        noStroke()
        fill(self.cor[0], self.cor[1], self.cor[2], self.cor[3])
        
        x = cenario.pixels(self.pos_x)
        y = cenario.pixels(self.pos_y)
        r = cenario.pixels(self.raio)
        
        textSize(18)
        text(self.nome, x - r, y - r)
        
        ellipseMode(CENTER)
        ellipse(x, y, 2 * r, 2 * r)
