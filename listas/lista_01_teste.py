#!/usr/bin/python
# coding:utf-8
import unittest as ut
import testutil as tu
import sys
import imp

#import referencia_01 as ref
import random as rd
import numpy as np
import numpy.random as npr

class Lista1(ut.TestCase):

	assercoes = 10

	@staticmethod
	def randlen():
		return npr.randint(0, 10)

	@tu.Relatorio(assercoes)
	def setUp(self):
		rd .seed(1234)
		npr.seed(1234)

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_parabola(self):
		a, b, c, x = npr.random(4) * 10 - 5
		y = a * x**2 + b * x + c
		self.assertAlmostEqual(mod.parabola(x, a, b, c), y)

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_modulo(self):
		x = npr.random() * 10 - 5
		y = np.abs(x)

		self.assertAlmostEqual(mod.modulo(x), y)

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_somatorio(self):
		l = npr.random(Lista1.randlen())
		s = sum(l)
		a = rd.random() * 10 - 5

		self.assertAlmostEqual(mod.somatorio(l), s)
		self.assertAlmostEqual(mod.somatorio(l, a), s + a)

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_minimo_e_maximo(self):
		l = npr.random(Lista1.randlen() + 1)
		min_x = min(l)
		max_x = max(l)

		self.assertEqual(mod.minimo(l), min_x)
		self.assertEqual(mod.maximo(l), max_x)

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_fatia(self):
		lista = npr.randint(0, 10, Lista1.randlen())
		a, b, i = npr.randint(-len(lista), len(lista), 3)
		if i == 0:
			i = 1	
		self.assertEqual(mod.fatia(lista, a, b, i), list(lista[a:b:i]))

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_conjunto(self):
		lista = npr.randint(0, 10, Lista1.randlen())
		self.assertEqual(set(mod.conjunto(lista)), set(lista))

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_remover_todos(self):
		lista = npr.randint(0, 10, Lista1.randlen() + 1)
		excluido = rd.choice(lista)
		filtrada = filter(lambda x : x != excluido, lista)
		self.assertEqual(mod.remover_todos(lista, excluido), filtrada)

	@tu.Relatorio(assercoes)
	def teste_separar(self):
		frases = ['Is there anybody out there?', ' ', 'Just:nod:if:you:can:hear:me']
		separadores = [' ', ' ', ':']
		for f, s in zip(frases, separadores):
			self.assertEqual(mod.separar(f, s), f.split(s))

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_mapear(self):
		lista = npr.randint(0, 10, Lista1.randlen())
		funcoes = [
			lambda x : x**2,
			lambda x : 3 * x,
			lambda x : str(x)
		]
		for f in funcoes:
			self.assertEqual(mod.mapear(f, lista), map(f, lista))

	@tu.Iteracoes(assercoes)
	@tu.Relatorio(assercoes)
	def teste_filtrar(self):
		lista = npr.randint(0, 10, Lista1.randlen())
		funcoes = [
			lambda x : x > 0,
			lambda x : x < 0,
			lambda x : x % 2
		]
		for f in funcoes:
			self.assertEqual(mod.filtrar(f, lista), filter(f, lista))

modulos = sys.argv[1:]
del sys.argv[1:]

if __name__ == '__main__':
	for path in modulos:
		mod = imp.load_source(path.replace('.py', ''), path)
		tu.abrir(path.replace('.py', '.html'))
		ut.main()
