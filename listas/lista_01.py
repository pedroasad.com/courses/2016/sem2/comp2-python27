# coding: utf-8
def parabola(x, a = 1, b = 0, c = 0):
	return a * x ** 2 + b * x + c

def modulo(x):
	if x < 0:
		return -x
	else:
		return x

def somatorio(lista, inicial = 0):
	soma = inicial
	for x in lista:
		soma += x
	return soma

def minimo(lista):
	minx = None
	for x in lista:
		if minx is None:
			minx = x
		elif x < minx:
			minx = x
	return minx

def maximo(lista):
	maxx = None
	for x in lista:
		if maxx is None:
			maxx = x
		elif x > maxx:
			maxx = x
	return maxx

#def amostras_inc(ini, fim, inc = 1):
#	amostras = []
#	x = ini
#	while x <= fim:
#		amostras.append(x)
#		x += inc
#	return amostras

def fatia(lista, ini = 0, fim = None, inc = 1):
	resultado = []
	
	if ini < 0:
		ini += len(lista)

	if fim < 0:
		fim += len(lista)
	elif fim is None:
		fim = len(lista)

	for i in range(ini, fim, inc):
		resultado.append(lista[i])
	return resultado

def amostras_inc(ini, fim, inc = 1):
	amostras = []
	for i in range(int((fim - ini) / inc) + 1):
		amostras.append(ini + i * inc)
	return amostras

def amostras_num(ini, fim, num):
	inc = float(fim - ini) / (num - 1)
	return amostras_inc(ini, fim, inc)

def conjunto(lista):
	conj = []
	for x in lista:
		if x not in conj:
			conj.append(x)
	return conj

def remover_todos(lista, valor):
	filtrada = []
	for x in lista:
		if x != valor:
			filtrada.append(x)
	return filtrada

def separar(texto, separador):
	strings = []
	parcial = ''
	for l in texto:
		if l == separador:
			strings.append(parcial)
			parcial = ''
		else:
			parcial += l
	strings.append(parcial)
	return strings

def juntar(strings, separador):
	texto = ''
	for s in strings:
		if texto:
			texto += separador + s
		else:
			texto += s
	return texto

def mapear(funcao, lista):
	mapeados = []
	for x in lista:
		mapeados.append(funcao(x))
	return mapeados

def filtrar(funcao, lista):
	filtrados = []
	for x in lista:
		if funcao(x):
			filtrados.append(x)
	return filtrados
