# coding: utf-8
import unittest as ut
from testutil import *

class VetorTeste(Vetor):
    def __repr__(self):
        return '(%f, %f, %f)' % (self.x, self.y, self.z)

class Teste(ut.TestCase):
    @relatorio
    def teste___init__(self):
        v1 = VetorTeste()
        v2 = VetorTeste(2)
        v3 = VetorTeste(2, 3)
        v4 = VetorTeste(2, 3, 4)
        self.assertEquals(repr
