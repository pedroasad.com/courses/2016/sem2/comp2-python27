=========================================
2015-2 - Computação 2 - Python - UFRJ/DCC
=========================================

.. toctree::

   Informações sobre este curso/material <README>
   modulos/index
   listas/index
   provas/index
   topicos/index
   desafios/index
   projetos/index

.. toctree::
   :hidden:

   teoricas
   praticas

Índices e tabelas
=================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

