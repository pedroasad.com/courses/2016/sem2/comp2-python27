class ModeloCinematico:
        def __init__(self, pos, vel = 0.0):
                self.pos = pos
                self.vel = vel
        
        def mover(self, dt, acel = 0):
                self.pos += self.vel * dt + acel * dt ** 2 / 2
                self.vel +=                 acel * dt

class Paraquedista(ModeloCinematico):
        def __init__(self, pos, vel = 0.0):
                ModeloCinematico.__init__(self, pos, vel)             
                self.aberto = False

        def __repr__(self):
            return '(%f, %f, %d)' % (self.pos, self.vel, self.aberto)

        def abrir_paraquedas(self):
                self.aberto = True
        
        def mover(self, dt, acel = 0):
                if self.aberto:
                        if self.vel < -1:
                                dt_vel = (-1 - self.vel) / 20
                                if dt_vel < dt:
                                        ModeloCinematico.mover(self, dt_vel, 20)
                                        ModeloCinematico.mover(self, dt - dt_vel)
                                else:
                                        ModeloCinematico.mover(self, dt, 20)
                        else:
                                ModeloCinematico.mover(self, dt)
                else:
                        ModeloCinematico.mover(self, dt, -10)

class ParaquedistaCauteloso(Paraquedista):
        def mover(self, dt, acel = 0):
                if not self.aberto and self.pos <= self.vel**2/40 + 200:
                        self.abrir_paraquedas()
                Paraquedista.mover(self, dt, acel)


p1 = ParaquedistaCauteloso(1000)
p2 = ParaquedistaCauteloso(1200)
p3 = ParaquedistaCauteloso(1000)

p1.mover(10)
p2.mover(10)
p3.mover(10)
print p1, p2, p3

p1.abrir_paraquedas()

p1.mover(2)
p2.mover(2)
p3.mover(2)
print p1, p2, p3

p3.abrir_paraquedas()

p1.mover(5)
p2.mover(5)
p3.mover(5)
print p1, p2, p3


